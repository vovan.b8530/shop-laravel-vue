<footer class="main-footer">
    <strong>Copyright &copy; 2018-{{now()->year}} <a href="{{route('main')}}">shop</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
{{--        <b>Version</b> 3.2.0--}}
    </div>
</footer>
