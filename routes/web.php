<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\TagController;
use Illuminate\Support\Facades\Route;


Route::get('/', MainController::class)->name('main');

Route::resource('categories', CategoryController::class);
Route::resource('tags', TagController::class);
Route::resource('colors', ColorController::class);
