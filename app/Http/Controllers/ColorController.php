<?php

namespace App\Http\Controllers;

use App\Http\Requests\ColorRequest;
use App\Models\Color;

class ColorController extends Controller
{
    public function index()
    {
        return Color::all();
    }

    public function store(ColorRequest $request)
    {
        return Color::create($request->validated());
    }

    public function show(Color $color)
    {
        return $color;
    }

    public function update(ColorRequest $request, Color $color)
    {
        $color->update($request->validated());

        return $color;
    }

    public function destroy(Color $color)
    {
        $color->delete();

        return response()->json();
    }
}
