<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagRequest;
use App\Models\Tag;
use App\Services\TagService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TagController extends Controller
{
    /**
     * @param  TagService  $service
     */
    public function __construct(protected TagService $service)
    {
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $tags = Tag::all();

        return view('tags.index', ['tags' => $tags]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('tags.create');
    }

    /**
     * @param  Tag  $tag
     * @return View
     */
    public function edit(Tag $tag): View
    {
        return view('tags.edit', ['tag' => $tag]);
    }

    /**
     * @param  Tag  $tag
     * @return View
     */
    public function show(Tag $tag): View
    {
        return view('tags.show', ['tag' => $tag]);
    }

    /**
     * @param  TagRequest  $request
     * @return RedirectResponse
     */
    public function store(TagRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $this->service->store($data);

        return redirect()->route('tags.index');
    }

    /**
     * @param  TagRequest  $request
     * @param  Tag  $tag
     * @return RedirectResponse
     */
    public function update(TagRequest $request, Tag $tag): RedirectResponse
    {
        $data = $request->validated();
        $this->service->update($data, $tag);

        return redirect()->route('tags.show', ['tag' => $tag]);
    }

    /**
     * @param  Tag  $tag
     * @return RedirectResponse
     */
    public function destroy(Tag $tag): RedirectResponse
    {
        $tag->delete();

        return redirect()->route('tags.index');
    }
}
