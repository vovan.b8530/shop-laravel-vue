<?php

namespace App\Services;

use App\Models\Category;

class CategoryService
{
    /**
     * @param $data
     * @return bool
     */
    public function store($data): bool
    {
        Category::firstOrCreate($data);
        return true;
    }

    /**
     * @param $data
     * @param  Category  $category
     * @return bool
     */
    public function update($data, Category $category): bool
    {
        return $category->update($data);
    }
}
