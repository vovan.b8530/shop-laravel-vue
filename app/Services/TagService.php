<?php

namespace App\Services;

use App\Models\Tag;

class TagService
{
    /**
     * @param $data
     * @return bool
     */
    public function store($data): bool
    {
        Tag::firstOrCreate($data);
        return true;
    }

    /**
     * @param $data
     * @param  Tag  $tag
     * @return bool
     */
    public function update($data, Tag $tag): bool
    {
        return $tag->update($data);
    }
}
